var interval = 3000;
var imageDir = "images/";

var imageNum = 0;
imageArray = new Array();
imageArray[imageNum++] = new imageItem(imageDir + "image-slider-1.jpg");
imageArray[imageNum++] = new imageItem(imageDir + "image-slider-2.jpg");
imageArray[imageNum++] = new imageItem(imageDir + "image-slider-3.jpg");
imageArray[imageNum++] = new imageItem(imageDir + "image-slider-4.jpg");
imageArray[imageNum++] = new imageItem(imageDir + "image-slider-5.JPG");
imageArray[imageNum++] = new imageItem(imageDir + "image-slider-6.JPG");
imageArray[imageNum++] = new imageItem(imageDir + "image-slider-7.JPG");
imageArray[imageNum++] = new imageItem(imageDir + "image-slider-8.JPG");
var totalImages = imageArray.length;

function imageItem(image_location) {
    this.image_item = new Image();
    this.image_item.src = image_location;
}

function get_ImageItemLocation(imageObj) {
    return(imageObj.image_item.src)
}

function getNextImageSrc() {
    imageNum = (imageNum+1) % totalImages;

    var new_image_src = get_ImageItemLocation(imageArray[imageNum]);
    return(new_image_src);
}

function switchImage(place) {
    var new_image_src = getNextImageSrc();
    document[place].src = new_image_src;
    var recur_call = "switchImage('" + place + "')";
    timerID = setTimeout(recur_call, interval);
}
